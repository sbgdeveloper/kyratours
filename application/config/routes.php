<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Frontend';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['about-us'] = 'Frontend/aboutUs';
$route['contact-us'] = 'Frontend/contactUs';
$route['tours/(:any)'] = 'Frontend/tour/$1';
$route['enquiry'] = 'Frontend/enquiry';
$route['subscribe'] = 'Frontend/subscribe';
$route['flight'] = 'Frontend/flight';
$route['corporate-travel'] = 'Frontend/corporate';
$route['travel-insurance'] = 'Frontend/insurance';
$route['car-rental'] = 'Frontend/carrental';
$route['visa'] = 'Frontend/visa';
$route['enterprise-tickets'] = 'Frontend/interpriseTickets';
$route['enterprise-visa'] = 'Frontend/interpriseVisa';
$route['virtual-desk'] = 'Frontend/virtualDesk';
$route['book-now'] = 'Frontend/bookNow';

$route['login'] = 'admin/login';
$route['logout'] = 'admin/logout';
$route['admin/dashboard'] = 'backend/dashboard';

/*User */
$route['admin/user/list']='UserController';
$route['admin/user/add']='UserController/add';
$route['admin/user/edit/(:any)']='UserController/edit/$1';
$route['admin/user/enable/(:any)']='UserController/enable/$1';
$route['admin/user/disable/(:any)']='UserController/disable/$1';
$route['admin/user/delete/(:any)']='UserController/delete/$1';
$route['admin/change-password']='UserController/changePassword';

/*Pages */
$route['admin/pages/list']='PagesController';
$route['admin/pages/add']='PagesController/add';
$route['admin/pages/edit/(:any)']='PagesController/edit/$1';
$route['admin/pages/enable/(:any)']='PagesController/enable/$1';
$route['admin/pages/disable/(:any)']='PagesController/disable/$1';
$route['admin/pages/delete/(:any)']='PagesController/delete/$1';

/*Settings */
$route['admin/settings/list']='SettingsController';
$route['admin/settings/add']='SettingsController/add';
$route['admin/settings/edit/(:any)']='SettingsController/edit/$1';
$route['admin/settings/enable/(:any)']='SettingsController/enable/$1';
$route['admin/settings/disable/(:any)']='SettingsController/disable/$1';
$route['admin/settings/delete/(:any)']='SettingsController/delete/$1';


/*Blogs*/
$route['admin/blog/list']='BlogController';
$route['admin/blog/add']='BlogController/add';
$route['admin/blog/edit/(:any)']='BlogController/edit/$1';
$route['admin/blog/enable/(:any)']='BlogController/enable/$1';
$route['admin/blog/disable/(:any)']='BlogController/disable/$1';
$route['admin/blog/delete/(:any)']='BlogController/delete/$1';

/*Products*/
$route['admin/product/list']='ProductController';
$route['admin/product/add']='ProductController/add';
$route['admin/product/edit/(:any)']='ProductController/edit/$1';
$route['admin/product/enable/(:any)']='ProductController/enable/$1';
$route['admin/product/disable/(:any)']='ProductController/disable/$1';
$route['admin/product/delete/(:any)']='ProductController/delete/$1';

/*Client*/
$route['admin/client/list']='ClientController';
$route['admin/client/add']='ClientController/add';
$route['admin/client/edit/(:any)']='ClientController/edit/$1';
$route['admin/client/enable/(:any)']='ClientController/enable/$1';
$route['admin/client/disable/(:any)']='ClientController/disable/$1';
$route['admin/client/delete/(:any)']='ClientController/delete/$1';

/*Category*/
$route['admin/category/list']='CategoryController';
$route['admin/category/add']='CategoryController/add';
$route['admin/category/edit/(:any)']='CategoryController/edit/$1';
$route['admin/category/enable/(:any)']='CategoryController/enable/$1';
$route['admin/category/disable/(:any)']='CategoryController/disable/$1';
$route['admin/category/delete/(:any)']='CategoryController/delete/$1';

/*Sub Category*/
$route['admin/subcategory/list']='SubcategoryController';
$route['admin/subcategory/add']='SubcategoryController/add';
$route['admin/subcategory/edit/(:any)']='SubcategoryController/edit/$1';
$route['admin/subcategory/enable/(:any)']='SubcategoryController/enable/$1';
$route['admin/subcategory/disable/(:any)']='SubcategoryController/disable/$1';
$route['admin/subcategory/delete/(:any)']='SubcategoryController/delete/$1';

/*Services*/
$route['admin/service/list']='ServicesController';
$route['admin/service/add']='ServicesController/add';
$route['admin/service/edit/(:any)']='ServicesController/edit/$1';
$route['admin/service/enable/(:any)']='ServicesController/enable/$1';
$route['admin/service/disable/(:any)']='ServicesController/disable/$1';
$route['admin/service/delete/(:any)']='ServicesController/delete/$1';

/*Service components*/
$route['admin/service-component/list']='ServicesComponentController';
$route['admin/service-component/add']='ServicesComponentController/add';
$route['admin/service-component/edit/(:any)']='ServicesComponentController/edit/$1';
$route['admin/service-component/enable/(:any)']='ServicesComponentController/enable/$1';
$route['admin/service-component/disable/(:any)']='ServicesComponentController/disable/$1';
$route['admin/service-component/delete/(:any)']='ServicesComponentController/delete/$1';

/*Gallery*/
$route['admin/gallery/list']='GalleryController';
$route['admin/gallery/add']='GalleryController/add';
$route['admin/gallery/edit/(:any)']='GalleryController/edit/$1';
$route['admin/gallery/enable/(:any)']='GalleryController/enable/$1';
$route['admin/gallery/disable/(:any)']='GalleryController/disable/$1';
$route['admin/gallery/delete/(:any)']='GalleryController/delete/$1';

/*Calculator*/
$route['admin/invoice/list']='InvoiceController';

$route['admin/invoice/add']='InvoiceController/add';
$route['admin/invoice/edit/(:any)']='InvoiceController/edit/$1';
$route['admin/invoice/enable/(:any)']='InvoiceController/enable/$1';
$route['admin/invoice/disable/(:any)']='InvoiceController/disable/$1';
$route['admin/invoice/delete/(:any)']='InvoiceController/delete/$1';
$route['admin/invoice/view/(:any)']='InvoiceController/view/$1';


/*Tours*/
$route['admin/tours/list']='ToursController';
$route['admin/tours/add']='ToursController/add';
$route['admin/tours/edit/(:any)']='ToursController/edit/$1';
$route['admin/tours/enable/(:any)']='ToursController/enable/$1';
$route['admin/tours/disable/(:any)']='ToursController/disable/$1';
$route['admin/tours/delete/(:any)']='ToursController/delete/$1';
$route['admin/tours/getlocations']='ToursController/getlocations';

/*Testimonials*/
$route['admin/testimonials/list']='TestimonialsController';
$route['admin/testimonials/add']='TestimonialsController/add';
$route['admin/testimonials/edit/(:any)']='TestimonialsController/edit/$1';
$route['admin/testimonials/enable/(:any)']='TestimonialsController/enable/$1';
$route['admin/testimonials/disable/(:any)']='TestimonialsController/disable/$1';
$route['admin/testimonials/delete/(:any)']='TestimonialsController/delete/$1';