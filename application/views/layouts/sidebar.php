<aside class="main-sidebar">
	<section class="sidebar">
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
			<li><a href="<?=base_url()?>admin/dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
			<li><a href="<?=base_url()?>admin/pages/list"><i class="fa fa-file"></i> <span>Pages</span></a></li>
			<li><a href="<?=base_url()?>admin/blog/list"><i class="fa fa-newspaper-o"></i> <span>Blogs</span></a></li>
			<li><a href="<?=base_url()?>admin/user/list"><i class="fa fa-user"></i> <span>User Managements</span></a></li>
			<li><a href="<?=base_url()?>admin/testimonials/list"><i class="fa fa-file"></i> <span>Testimonials</span></a></li>			
			<li><a href="<?=base_url()?>admin/tours/list"><i class="fa fa-plane"></i> <span>Tours</span></a></li>
			
		</ul>
	</section>
</aside>