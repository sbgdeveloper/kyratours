<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Testimonials Management
			<small>Edit Testimonial</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/testimonials/list">Testimonials</a></li>
			<li class="active">Edit Testimonial</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<div class="row">
					<form method="post" enctype="multipart/form-data">
						
						<div class="col-md-12">
							<div class="form-group">
								<label>Image</label>
								<input type='file' class="form-control" name="image" onchange="readURL(this);" value="<?=$Record['image']?>" >
								<?php if (isset($Record['image'])): ?>
									<img id="blah" src="<?= base_url('uploads/testimonials')?>/<?=$Record['image']?>" alt="your image" class="pre-img" />
									<?php else: ?>
										<img id="blah" src="http://placehold.it/180" alt="your image" class="pre-img" />
									<?php endif ?>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Client Name *</label>
									<input type="text" name="name" class="form-control" placeholder="Enter Name" value="<?=$Record['name']?>" required>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Job Title *</label>
									<input type="text" name="title" class="form-control" placeholder="Enter  Job Title" value="<?=$Record['title']?>" required>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label>Company </label>
									<input type="text" name="company" class="form-control" value="<?=$Record['company']?>" placeholder="Enter Company">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Testimonial Text </label>
									<textarea name="description" class="form-control" placeholder="Enter Testimonial Text" rows="5"><?=$Record['description']?></textarea>
							</div>
						</div>

						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>

				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section>
<!-- /.content -->
</div>
<?php $this->load->view('layouts/footer');?>