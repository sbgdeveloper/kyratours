<?php if($this->session->flashdata('email_subscribe')): ?>
<section class="mb-5" id="displayResponse">
    <?php else: ?>
<section class="mb-5">
    <?php endif ?>
    <div class="container">



        <h2 class="clr-blue text-center mt-5 mb-3 text-uppercase">Subscribe To Our Newsletter</h2>
        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam sit iste esse assumenda amet aperiam exercitationem, ea animi blanditiis recusandae! Ratione voluptatum molestiae adipisci, beatae obcaecati Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
        <form method="post" action="<?=base_url()?>subscribe">
            <div class="col-lg-8 offset-lg-2">
                <div class="row py-4">
                    <div class="col-lg-8 mb-3">
                        <input type="email" name="email" class="form-control brd-box brdrds0" placeholder="Please Enter Your Email Id" required>
                    </div>
                    <div class="col-lg-4">
                        <button class="btn btn-subscribe">Subscribe</button>
                    </div>         
                </div>
            </div>
        </form>

         <?php if($this->session->flashdata('email_subscribe')): ?>
            <div class="alert alert-info">
                <?php echo $this->session->flashdata('email_subscribe') ?>
            </div>
        <?php endif ?>
        <?php if($this->session->flashdata('email_subscribe_err')): ?>
            <div class="alert alert-danger">
                <strong>Info!</strong> <?php echo $this->session->flashdata('email_subscribe_err') ?>
            </div>
        <?php endif ?>

    </div>
</section>