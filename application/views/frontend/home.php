<?php $this->load->view('frontend/layouts/header');?>
    <div id="demo" class="carousel slide m100" data-ride="carousel">
        <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
        </ul>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="img/banner.png" alt="Banner" class="img-fluid">
                <div class="carousel-caption text-left">
                    <p class="f16">Book Your Desired Travel Place</p>
                    <h2 class="first-heading">Travel For Life</h2>
                </div>
            </div>
            <div class="carousel-item">
                <img src="img/banner2.png" alt="Banner" class="img-fluid">
                <div class="carousel-caption text-right mobile-right">
                    <p class="f16">Book Your Desired Travel Place</p>
                    <h2 class="first-heading">Travel For Life</h2>
                </div>
            </div>
            <div class="carousel-item">
                <img src="img/banner3.png" alt="Banner" class="img-fluid">
                <div class="carousel-caption text-left">
                    <p class="f16">Book Your Desired Travel Place</p>
                    <h2 class="first-heading">Travel For Life</h2>
                </div>
            </div>
        </div>
    </div>

<section class="my-5">
    <div class="container">
        <h2 class="clr-red text-center"><span class="clr-blue"> Why</span> Choose Us ?</h2>
        <p class="mt-3 text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam sit iste esse assumenda amet aperiam exercitationem, ea animi blanditiis recusandae! Ratione voluptatum molestiae adipisci, beatae obcaecati.</p>
        <div class="row text-center mt-5 pb-3" id="why-us">
            <div class="col-lg-3">
                <img class="img-fluid" src="img/tours.png" alt="">
                <h4 class="mt-3 clr-red">2000+ Tours</h4>
                <p class="mt-2">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam
                </p>
            </div>
            <div class="col-lg-3">
                <img class="img-fluid" src="img/value-packages.png" alt="">
                <h4 class="mt-3 clr-red">Value Of Packages</h4>
                <p class="mt-2">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam
                </p>
            </div>
            <div class="col-lg-3">
                <img class="img-fluid" src="img/happy-tourist.png" alt="">
                <h4 class="mt-3 clr-red">99% Happy Tourist</h4>
                <p class="mt-2">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam
                </p>
            </div>
            <div class="col-lg-3">
                <img class="img-fluid" src="img/happy-serve.png" alt="">
                <h4 class="mt-3 clr-red">Happy To Serve</h4>
                <p class="mt-2">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 p-0 text-center">
                <img class="img-fluid" src="img/why-us.png" alt="About Us">
            </div>
            <div class="col-lg-7 bg-grey px-5">
                <h3 class="pt-5">Welcome To KYRA Tours & Travel</h3>
                <p class="pt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam sit iste esse assumenda amet aperiam exercitationem, ea animi blanditiis recusandae! Ratione voluptatum molestiae adipisci, beatae obcaecati. Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                <a class="btn btn-sm btn-booknow mb-3" href="<?=base_url()?>about-us">Read More</a>
            </div>
        </div>
    </div>
</section>

<section class="pt-4">
    <div class="container">
        <h2 class="clr-red text-center">Domestic Tours</h2>
        <div class="row">
            <?php foreach ($tours as $tour): ?>
                <?php if ($tour['tour_type'] == '1'): ?>
            <div class="col-lg-4 col-md-6 img-hover-zoom mobile-center mb-3">
                <img class="img-fluid" src="uploads/tours/<?= $tour['tour_image'] ?>" alt="About Us">
                <h6 class="float-right pr-4 bottom-right"><?= $tour['name'] ?></h6>
            </div>
            <?php endif ?>
            <?php endforeach ?>
            <div class="col-lg-12">
                <a href="<?=base_url()?>tours/domestic" class="view-more mr-3">View More Tours <i class="fa fa-long-arrow-right"></i></a>
            </div>
        </div>
        <h2 class="clr-red text-center mt-5">International Tours</h2>
        <div class="row">
            <?php foreach ($tours as $tour): ?>
                <?php if ($tour['tour_type'] == '2'): ?>
            <div class="col-lg-4 col-md-6 img-hover-zoom mobile-center mb-3">
                <img class="img-fluid" src="uploads/tours/<?= $tour['tour_image'] ?>" alt="About Us">
                <h6 class="float-right pr-4 bottom-right"><?= $tour['name'] ?></h6>
            </div>
            <?php endif ?>
            <?php endforeach ?>
            
            <div class="col-lg-12">
                <a href="<?=base_url()?>tours/international" class="view-more mr-3">View More Tours <i class="fa fa-long-arrow-right"></i></a>
            </div>
        </div>
    </div>
</section>
<section class="my-5 ">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 mb-3">
                <div class="card brdrds0 brd-box">
                    <div class="px-5 card-header brdrds0 clr-white bg-blue"><b>Car Rental</b></div>
                    <div class="px-5 card-body min-hgt">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam sit iste esse assumenda amet aperiam exercitationem, ea animi blanditiis recusandae! Ratione voluptatum molestiae adipisci, beatae obcaecati. Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                        <a class="btn btn-sm btn-booknow float-right" href="#">Read More</a> 
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card brdrds0 brd-box">
                    <div class="card-header brdrds0 clr-white bg-blue px-5"><b>Corporate Travel</b></div>
                    <div class="card-body min-hgt pl-3">
                        <ul class="ul-square">
                            <li class="py-3">Enterprise Tickets</li>
                            <li class="pb-3">Enterprise VISA</li>
                            <li class="pb-3">Virtual Desk</li>
                            <li class="">Car Rental</li>
                        </ul>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('frontend/subscribe');?>

<?php $this->load->view('frontend/layouts/footer');?>