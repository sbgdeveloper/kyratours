<?php $this->load->view('frontend/layouts/header');?>
<section class="m100"  id="flights">
    <div class="container">
        <h1 class="clr-white text-center f54 p100 text-uppercase"> Flight Booking</h1>
    </div>
    <hr class="ft-hr m100">
    <div class="container">
        <div class="text-center">
            <a href="<?=base_url()?>"><span class="brdcum">HOME </span> </a>
            <i class="fa fa-angle-right clr-white f18 pl-2"></i>
            <span class="brdcum text-uppercase">FLIGHT BOOKING</span>
        </div>
    </div>
</section>

<section class="pt-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="pt-5 clr-red">Flight Booking</h3>
                <p class="pt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam sit iste esse assumenda amet aperiam exercitationem, ea animi blanditiis recusandae! Ratione voluptatum molestiae adipisci, beatae obcaecati. Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam sit iste esse assumenda amet aperiam exercitationem, ea animi blanditiis recusandae! Ratione voluptatum molestiae adipisci, beatae obcaecati. Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam sit iste esse assumenda amet aperiam exercitationem, ea animi blanditiis recusandae! Ratione voluptatum molestiae adipisci, beatae obcaecati. Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                <p>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
                </p>
            </div>
        </div>
    </div>
</section>

<section class="pt-4">
    <div class="container">
        <div class="col-lg-12 brd-box brd-rds py-3">
            <h2 class="clr-red pl-3">Domestic Tours</h2>
            <div class="row">
                <?php foreach ($tours as $tour): ?>
                <?php if ($tour['tour_type'] == '1'): ?>
                <div class="col-lg-4 col-md-6 img-hover-zoom mobile-center mb-3">
                    <img class="img-fluid" src="uploads/tours/<?= $tour['tour_image'] ?>" alt="About Us">
                    <h6 class="float-right pr-4 bottom-right"><?= $tour['name'] ?></h6>
                </div>
                <?php endif ?>
                <?php endforeach ?>
                <div class="col-lg-12">
                    <a href="<?=base_url()?>tours/domestic" class="view-more mr-3">View More Tours <i class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>
        </div>
        <div class="col-lg-12 brd-box brd-rds py-3 mt-3">
            <h2 class="clr-red pl-3">International Tours</h2>
            <div class="row">
                <?php foreach ($tours as $tour): ?>
                <?php if ($tour['tour_type'] == '2'): ?>
                <div class="col-lg-4 col-md-6 img-hover-zoom mobile-center mb-3">
                    <img class="img-fluid" src="uploads/tours/<?= $tour['tour_image'] ?>" alt="About Us">
                    <h6 class="float-right pr-4 bottom-right"><?= $tour['name'] ?></h6>
                </div>
                <?php endif ?>
                <?php endforeach ?>

                <div class="col-lg-12">
                    <a href="<?=base_url()?>tours/international" class="view-more mr-3">View More Tours <i class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('frontend/enquiry');?>
<?php $this->load->view('frontend/subscribe');?>
<?php $this->load->view('frontend/layouts/footer');?>