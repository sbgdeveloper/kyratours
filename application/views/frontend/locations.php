<?php $this->load->view('frontend/layouts/header');?>
    <section class="m100"  id="domestic">
        <div class="container">
            <h1 class="clr-white text-center f54 p100 text-uppercase"> <?=$type?> Packages</h1>
        </div>
        <hr class="ft-hr m100">
        <div class="container">
            <div class="text-center">
               <a href="<?=base_url()?>"><span class="brdcum">HOME </span> </a>
                <i class="fa fa-angle-right clr-white f18 pl-2"></i>
                <span class="brdcum">Tour Packages</span>
                <i class="fa fa-angle-right clr-white f18 pl-2"></i>
                <span class="brdcum"><?=$type?> Packages</span>

            </div>
        </div>
    </section>

    <section class="pt-4 bg-grey">
        <div class="container">
            <h2 class="clr-red text-center"><?=$type?></h2>
            <div class="row">
                <?php foreach ($tours as $tour): ?>
                <div class="col-lg-4 pb-5 col-md-6 mobile-center">
                    <div class="img-box">
                        <img src="<?=base_url()?>uploads/tours/<?= $tour['tour_image'] ?>" class="img-fluid">
                        <div class="bg-white d-inline prev-title">
                            <h5 class="clr-red py-2 pl-3 pr-5 my-auto"><?= $tour['name'] ?></h5>
                            <a href="#" class="place-view ml-5 my-auto" >View</a>
                        </div>
                        
                        
                    </div>
                </div>
                <?php endforeach ?>
                <div class="col-lg-12 ">
                    <ul class="pagination float-right">
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item " ><a class="page-link" href="#">2</a></li>
                        
                        <li class="page-item"><a class="page-link" href="#">Next</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <?php $this->load->view('frontend/enquiry');?>
<?php $this->load->view('frontend/subscribe');?>
<?php $this->load->view('frontend/layouts/footer');?>