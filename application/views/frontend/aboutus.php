<?php $this->load->view('frontend/layouts/header');?>
    <section class="m100"  id="about-us">
        <div class="container">
            <h1 class="clr-white text-center f54 p100"> About Us </h1>
        </div>
        <hr class="ft-hr m100">
        <div class="container">
            <div class="text-center">
                <a href="<?=base_url()?>"><span class="brdcum">HOME </span> </a>
                <i class="fa fa-angle-right clr-white f18 pl-2"></i>
                <span class="brdcum">ABOUT US</span>
            </div>
        </div>
    </section>

    <section class="pt-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 pt-5">
                    <img class="img-fluid" src="img/about.png" alt="About Us">
                </div>
                <div class="col-lg-7 px-5">
                    <h3 class="pt-5 clr-red">Welcome To KYRA Tours & Travel</h3>
                    <p class="pt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam sit iste esse assumenda amet aperiam exercitationem, ea animi blanditiis recusandae! Ratione voluptatum molestiae adipisci, beatae obcaecati. Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam sit iste esse assumenda amet aperiam exercitationem, ea animi blanditiis recusandae! Ratione voluptatum molestiae adipisci, beatae obcaecati. Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam sit iste esse assumenda amet aperiam exercitationem, ea animi blanditiis recusandae! Ratione voluptatum molestiae adipisci, beatae obcaecati. Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                </div>
            </div>
        </div>
    </section>
    <section class="my-5 ">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mb-3">
                    <div class="row">
                        <div class="col-lg-4 text-right">
                            <img class="img-fluid" src="img/journey.svg" alt="Know Better">
                        </div>
                        <div class="col-lg-8">
                            <h4 class="clr-blue pt-2">Know Better</h4>
                            <p>
                                Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-3">
                    <div class="row">
                        <div class="col-lg-4 text-right">
                            <img class="img-fluid" src="img/booking.svg" alt="Book Better">
                        </div>
                        <div class="col-lg-8">
                            <h4 class="clr-blue pt-2">Book Better</h4>
                            <p>
                                Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-3">
                    <div class="row">
                        <div class="col-lg-4 text-right">
                            <img class="img-fluid" src="img/go.svg" alt="Go Better">
                        </div>
                        <div class="col-lg-8">
                            <h4 class="clr-blue pt-2">Go Better</h4>
                            <p>
                                Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-grey py-5">
        <div class="container text-center">
            <h2 class="clr-red mb-3">Testimonials</h2>
            <p>Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            <p><span class="undlne">&nbsp;</span></p>
            
            <div class="row mt-5">
                <?php foreach ($testimonials as $testimonial): ?>
                <div class="col-lg-4 mb-3">
                    <div class="mx-4 testim-box px-4 pt-4 pb-5">
                        <p><?= $testimonial['description'] ?></p>
                    </div>
                    <div class="name-tag"><?= $testimonial['name'] ?></div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
    </section>
    <?php $this->load->view('frontend/subscribe');?>
   
   <?php $this->load->view('frontend/layouts/footer');?>