<?php $this->load->view('frontend/layouts/header');?>
<section class="m100"  id="contact-us">
    <div class="container">
        <h1 class="clr-white text-center p150 text-uppercase"> Get In Touch</h1>
    </div>
</section>

<section class="mt200">
    <div class="container brd-box bg-white">
        <div class="row">
            <div class="col-lg-7 px-5">
                <?php if($this->session->flashdata('email_sent')): ?>
                <div class="alert alert-info">
                    <?php echo $this->session->flashdata('email_sent') ?>
                </div>
                <?php endif ?>
                <?php if($this->session->flashdata('email_sent_err')): ?>
                <div class="alert alert-danger">
                    <strong>Info!</strong> <?php echo $this->session->flashdata('email_sent_err') ?>
                </div>
                <?php endif ?>
                <br>
                <h3 class="pt-5 clr-red">Send a Message</h3>
                <form method="post">
                    <div class="row p-3">
                        <div class="col-lg-6 mb-3">
                            <div class="form-group contact-form">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" required>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-3">
                            <div class="form-group contact-form">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Your Email" required>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-3">
                            <div class="form-group contact-form">
                                <input type="tel" class="form-control" id="phone" name="phone" placeholder="Your Mobile" required onkeyup="if (/\D/g.test(this.value))
                                    this.value = this.value.replace(/\D/g, '')">
                            </div>
                        </div>
                        <div class="col-lg-6 mb-3">
                            <div class="form-group contact-form">
                                <input type="text" class="form-control" id="city" name="city" placeholder="Your City" required>
                            </div>
                        </div>
                        <div class="col-lg-12 mb-5">
                            <div class="form-group contact-form">
                                <input type="text" class="form-control" id="destination" name="destination" placeholder="Your Destination" required>
                            </div>
                        </div>
                        <div class="col-lg-12 mb-3">
                            <div class="form-group contact-form">
                                <textarea class="form-control"  id="msg" name="msg" rows="2" placeholder="Your Message" required></textarea>
                            </div>
                        </div>
                        <div class="col-lg-12 text-right mt-3">
                            <button type="submit" class="btn contact-submit pr-3"><i class="fa fa-paper-plane pr-2" aria-hidden="true"></i>Submit</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-5 px-5 pb-3 clr-white bg-blue">
                <br>
                <h3 class="pt-5">Contact Info</h3>
                <div class="my-3">
                    <span><i class="fa fa-phone pr-3"></i>+91 9876543210</span>
                </div>
                <div class="my-4">
                    <span><i class="fa fa-envelope pr-3"></i>contact@kyratour.com</span>
                </div>
                <div class="my-3">
                    <span><i class="fa fa-map-marker pr-3"></i>5, Mont Vert Marc, <br><span class="pl-4">Pashan Sus Road,</span> <br> <span class="pl-4">Pune 411021</span></span>
                </div>
                <span>
                    <a href="#"><img src="img/whatsapp.png" class="img-fluid pr-3" alt="KYRA"></a>
                    <a href="#"><img src="img/fb.png" class="img-fluid pr-3" alt="KYRA"></a>
                    <a href="#"><img src="img/link.png" class="img-fluid pr-3" alt="KYRA"></a>
                    <a href="#"><img src="img/twitter.png" class="img-fluid pr-3" alt="KYRA"></a>
                    <a href="#"><img src="img/youtube.png" class="img-fluid pr-3" alt="KYRA"></a>
                    <a href="#"><img src="img/insta.png" class="img-fluid" alt="KYRA"></a>
                </span>
            </div>
        </div>
    </div>
</section>



<?php $this->load->view('frontend/subscribe');?>
<?php $this->load->view('frontend/layouts/footer');?>