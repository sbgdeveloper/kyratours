<section class="mb-5" >
    <div class="container">
        <h2 class="clr-blue text-center">Get Quick Enquiry</h2>
        <div class="row mt-4">
            <form method="post" action="<?=base_url()?>enquiry">
                <div class="col-lg-8 offset-lg-2 brd-box">
                    <div class="row p-5">
                        <div class="col-lg-6">
                            <div class="form-group enq-form">
                                <label class="enq-label" for="name">Your Name</label>
                                <input type="text" class="form-control" id="name" name="name" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group enq-form">
                                <label for="email">Your Email</label>
                                <input type="email" class="form-control" id="email" name="email" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group enq-form">
                                <label class="enq-label" for="phone">Your Mobile</label>
                                <input type="tel" class="form-control" id="phone" name="phone" required onkeyup="if (/\D/g.test(this.value))
                                    this.value = this.value.replace(/\D/g, '')">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group enq-form">
                                <label class="enq-label" for="city">City</label>
                                <input type="text" class="form-control" id="city" name="city" required>
                            </div>
                        </div>
                        <?php if($this->session->flashdata('email_sent')): ?>
                            <div class="col-lg-12" id="displayResponse">
                                <?php else: ?>
                                    <div class="col-lg-12">
                                    <?php endif ?>
                                    <div class="form-group enq-form">
                                        <label class="enq-label" for="destination">Destination</label>
                                        <input type="text" class="form-control" id="destination" name="destination" required>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group enq-form">
                                        <label class="enq-label" for="msg">Your Message</label>
                                        <textarea class="form-control"  id="msg" name="msg" rows="4" required></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 text-center mt-3">
                                    <button type="submit" class="btn btn-submit">send Message</button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <?php if($this->session->flashdata('email_sent')): ?>
                        <div class="alert alert-info text-center">
                            <?php echo $this->session->flashdata('email_sent') ?>
                        </div>
                    <?php endif ?>
                    <?php if($this->session->flashdata('email_sent_err')): ?>
                        <div class="alert alert-danger">
                            <strong>Info!</strong> <?php echo $this->session->flashdata('email_sent_err') ?>
                        </div>
                    <?php endif ?>

                </div>
            </div>

        </section>