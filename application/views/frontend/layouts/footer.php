<footer class="py-3 bg-blue">
    <div class="container clr-white">
        <div class="row">
            <div class="col-lg-3 pt-4">
                <img src="<?=base_url()?>img/logo.png" class="img-fluid pb-3" alt="KYRA">
                <h5>Contact Us</h5>
                <p><b>Phone</b> : +91-93706 93705 </p>
                <p><b>Email</b> : manmeet@kyratours.com </p>
                <p><b>Address</b> : 5, Mont Vert Marc, Pashan Sus Road, Pune 411021</p>
                <span>
                    <a href="#"><img src="<?=base_url()?>img/whatsapp.png" class="img-fluid pr-2" alt="KYRA"></a>
                    <a href="#"><img src="<?=base_url()?>img/fb.png" class="img-fluid pr-2" alt="KYRA"></a>
                    <a href="#"><img src="<?=base_url()?>img/link.png" class="img-fluid pr-2" alt="KYRA"></a>
                    <a href="#"><img src="<?=base_url()?>img/twitter.png" class="img-fluid pr-2" alt="KYRA"></a>
                    <a href="#"><img src="<?=base_url()?>img/youtube.png" class="img-fluid pr-2" alt="KYRA"></a>
                    <a href="#"><img src="<?=base_url()?>img/insta.png" class="img-fluid pr-2" alt="KYRA"></a>
                    
                </span>
            </div>
            <div class="col-lg-3 pt-4">
                <h5>General Links</h5>
                <ul class="list-unstyled mt-4">
                    <li><a href="<?=base_url()?>" class="footer-link">Home</a></li>
                    <li><a href="<?=base_url()?>about-us" class="footer-link">About Us</a></li>
                    <li><a href="<?=base_url()?>tours/international" class="footer-link">Tour Packages</a></li>
                    <li><a href="<?=base_url()?>contact-us" class="footer-link">Contact Us</a></li>
                    <li><a href="<?=base_url()?>contact-us" class="footer-link">Enquiry</a></li>
                </ul>
            </div>
            <div class="col-lg-3 pt-4">
                <h5>Tour By Destination</h5>
                <ul class="list-unstyled mt-4">
                    <li><a href="<?=base_url()?>tours/domestic" class="footer-link">Domestic Tours</a></li>
                    <li><a href="<?=base_url()?>tours/international" class="footer-link">International Tours</a></li>
                </ul>
            </div>
            <div class="col-lg-3 mt-4">
                <h5>Extra Services</h5>
                <ul class="list-unstyled mt-4">
                    <li><a href="<?=base_url()?>visa" class="footer-link">VISA </a></li>
                    <li><a href="<?=base_url()?>travel-insurance" class="footer-link">Insurance</a></li>
                    <li><a href="<?=base_url()?>enterprise-tickets" class="footer-link">Enterprise Tickets</a></li>
                    <li><a href="<?=base_url()?>virtual-desk" class="footer-link">Virtual Desk</a></li>
                    <li><a href="<?=base_url()?>car-rental" class="footer-link">Car Rental</a></li>
                </ul>
            </div>
        </div>
    </div>
    <hr class="ft-hr">
    <div class="container">
        <p class="m-0 text-center text-white small"><span class="copyright"> &copy; All Rights Reserved By KYRA Tours And Travels | Powered By <a href="https://www.enayble.com/" target="_blank" class="enayble">Enayble.com</a></span></p>
    </div>
</footer>
<script src="<?=base_url()?>vendor/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('html, body').animate({
            scrollTop: $("#displayResponse").offset().top
        }, 500);
    });
  </script>
</body>
</html>
