<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="img/logo.png">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home Page | KYRA Travels</title>
    <link href="<?=base_url()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="<?=base_url()?>css/custom.css" rel="stylesheet">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
        <div class="container">
            <a class="navbar-brand" href="<?=base_url()?>">
                <img src="<?=base_url()?>img/logo.png" class="img-fluid">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>about-us">About Us</a>
                    </li>
                    <li class="nav-item">
                        <div class="dropdown drpdwn">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                                Tour Packages
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="<?=base_url()?>tours/domestic">Domestic</a>
                                <a class="dropdown-item" href="<?=base_url()?>tours/international">International</a>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>corporate-travel">Corporate Travel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>car-rental">Car Rental</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>flight">Flight</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>visa">VISA</a>
                    </li>
                    <li class="nav-item">
                        <div class="dropdown drpdwn">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                                Our Services
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="<?=base_url()?>enterprise-tickets">Enterprise Tickets</a>
                                <a class="dropdown-item" href="<?=base_url()?>enterprise-visa">Enterprise Visas</a>
                                <a class="dropdown-item" href="<?=base_url()?>virtual-desk">Virtual Desk</a>
                                <a class="dropdown-item" href="<?=base_url()?>travel-insurance">Travel Insurance</a>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>contact-us">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-sm btn-booknow" href="<?=base_url()?>book-now">Book Now</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>