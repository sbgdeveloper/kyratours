<?php $this->load->view('frontend/layouts/header');?>

    <section class="m100"  id="insurance">
        <div class="container">
            <h1 class="clr-white text-center f54 p100 text-uppercase"> Travel Insurance</h1>
        </div>
        <hr class="ft-hr m100">
        <div class="container">
            <div class="text-center">
                <a href="<?=base_url()?>"><span class="brdcum">HOME </span> </a>
                <i class="fa fa-angle-right clr-white f18 pl-2"></i>
                <span class="brdcum text-uppercase">Travel Insurance</span>
            </div>
        </div>
    </section>

    <section class="pt-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 brd-box brd-rds p-4">
                    <h3 class="clr-red">Travel Insurance</h3>
                    <img src="img/travels-insurance.png" class="img-fluid p-3 float-right">
                    <p class="pt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam sit iste esse assumenda amet aperiam exercitationem, ea animi blanditiis recusandae! Ratione voluptatum molestiae adipisci, beatae obcaecati. Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam sit iste esse assumenda amet aperiam exercitationem, ea animi blanditiis recusandae! Ratione voluptatum molestiae adipisci, beatae obcaecati. Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam sit iste esse assumenda amet aperiam exercitationem, ea animi blanditiis recusandae! Ratione voluptatum molestiae adipisci, beatae obcaecati. Lorem ipsum dolor sit amet,beatae obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                    <p>
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
                    </p>
                </div>
            </div>
        </div>
    </section>

    
    
    <?php $this->load->view('frontend/enquiry');?>
    <?php $this->load->view('frontend/subscribe');?>
    <?php $this->load->view('frontend/layouts/footer');?>