<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Invoice Management
      
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?=base_url()?>admin/invoice/list">Invoice List</a></li>
      <li class="active">Invoice</li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title"><?=$pagetitle;?></h3>
      </div>
      <div class="box-body">

        <p>
          To, Mr. <?=$Record['customer_name']?>. Ph- <?=$Record['customer_phone']?>.
        </p>
        <p>

          Subject:  Quotation for your requirement of Design, Manufacturing & Installation of Pre-Engineering Building for Standard Box Building at <?=$Record['location']?>.
        </p>

        <br>
        
        <table class="table table-bordered">

          <tbody>

            <tr>
              <th scope="row">1</th>
              <td>Width</td>
              <td><?=$Record['building_width']?> M O/O of Steel Line.</td>
              
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Length</td>
              <td><?=$Record['building_length']?> M O/O of Steel Line.</td>
              
            </tr>
            <tr>
              <th scope="row">3</th>
              <td>Height</td>
              <td><?=$Record['building_height']?> M Clear Ht.</td>
              
            </tr>
            <tr>
              <th scope="row">4</th>
              <td>Roof slope / future Expansion</td>
              <td>1:10 / Nil.</td>
              
            </tr>
            <tr>
              <th scope="row">5</th>
              <td>Bracings</td>
              <td>Rod Bracings at Roof & Wall.</td>
              
            </tr>
            <tr>
              <th scope="row">6</th>
              <td>Roofing & Wall Sheet</td>
              <td>0.50 MM Pre-Panted Galvanized (PPGI) Sheets (Profiled sheets).</td>
              
            </tr>
            <tr>
              <th scope="row">7</th>
              <td>Brick Masonry (by others)</td>
              <td>B/W up-to 3.0 m & above sheeting from all 4 sides.</td>
              
            </tr>
            <tr>
              <th scope="row">8</th>
              <td>Gutter / Down Spouts</td>
              <td>Yes. Gutter & D/T pipe considered. </td>
              
            </tr>
            <tr>
              <th scope="row">9</th>
              <td>Mezzanine / Crane</td>
              <td>Nil. (Please contact with details.)</td>
              
            </tr>
            <tr>
              <th scope="row">10</th>
              <td>Framed Openings / Canopy</td>
              <td>Nil. (Please contact with details.)</td>
              
            </tr>
            <tr>
              <th scope="row">11</th>
              <td>Sky Lights</td>
              <td>Polycarbonate sheets 2 mm Thk. 3-5 % of Roof area</td>
              
            </tr>
            <tr>
              <th scope="row">12</th>
              <td>Turbo Fans</td>
              <td>600 mm dia throat. Standard.</td>
              
            </tr>
            <tr>
              <th scope="row">13</th>
              <td>Design Considerations</td>
              <td>Code – AISC / MBMA. With standard loading.</td>
              
            </tr>
            <tr>
              <th scope="row">14</th>
              <td>Material Specifications</td>
              <td>As per Industrial Standard. </td>
              
            </tr>
            <tr>
              <th scope="row">15</th>
              <td>Schedule</td>
              <td>Can be discussed.</td>
              
            </tr>

            
          </tbody>
        </table>


        <br>

        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col">Building</th>
              <th scope="col">Supply (Rs.)</th>
              <th scope="col">Erection (Rs.)</th>
            </tr>
          </thead>
          <tbody>

            <tr>
              <th scope="row">Basic Price of Steel building</th>
              <td><?=$Record['supply_price']?></td>
              <td><?=$Record['erection_price']?></td>
              
            </tr>
            <tr>
              <th colspan="2">Total Price for Design, Manufacturing, Transportation & Erection of  Steel bldg (INR):</th>
              <td><?=$Record['total_price']?></td>
              
            </tr>
          </tbody>
        </table>


      </div>
    </div>
  </div>
</section>
</div>
<?php $this->load->view('layouts/footer');?>