<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Gallery Management
			<small>Add Gallery Image</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/gallery/list">Gallery</a></li>
			<li class="active">Add Gallery Image</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<?php if($this->session->flashdata('msg')): ?>
					<div class="alert alert-info">
						<strong>Info!</strong> <?php echo $this->session->flashdata('msg') ?>
					</div>
				<?php endif ?>
				<div class="row">
					<form method="post" enctype="multipart/form-data">
						<div class="col-md-12">
							<div class="form-group">
								<label>Service *</label>
								<select name="service_id" class="form-control select2" data-placeholder="" style="width: 100%;" required>
									<option value="">Select</option>
									<?php foreach ($Records as $Record): ?>
									<option value="<?php echo $Record['id'] ?>"><?php echo $Record['service_title'] ?>										
									</option>
								<?php endforeach ?>									
								</select>
							</div>
						</div>

						
						<div class="col-md-12">
							<div class="form-group">
								<label>Gallery Image *</label>
								<input type='file'  name="gallery_image" onchange="readURL(this);" required>	
								<img id="blah" src="http://placehold.it/180" alt="your image" class="pre-img" />
							</div>
						</div>
						
						
						<div class="col-md-12">
							<div class="form-group">
								<label>Status</label>
								<select name="status" class="form-control select2" data-placeholder="" style="width: 100%;" required>
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>
						
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<?php $this->load->view('layouts/footer');?>