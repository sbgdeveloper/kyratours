<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Tours Management
			<small>Add Tour</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/tours/list">Tours</a></li>
			<li class="active">Add Tour</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<?php if($this->session->flashdata('msg')): ?>
					<div class="alert alert-info">
						<strong>Info!</strong> <?php echo $this->session->flashdata('msg') ?>
					</div>
				<?php endif ?>
				<div class="row">
					<form method="post" enctype="multipart/form-data">

						<div class="col-md-12">
							<div class="form-group">
								<label>Tour Type *</label>
								<select name="tour_type" id="tour_type" class="form-control select2" data-placeholder="" style="width: 100%;" required onchange="getRegion();">
									<option value="">Select</option>
									<option value="1">Domestic</option>
									<option value="2">International</option>
								</select>
							</div>
						</div>

						<div class="col-md-12" id="country_div">
							<div class="form-group">
								<label>Country *</label>
								<select name="country_id" id="country_id" class="form-control select2" data-placeholder="" style="width: 100%;" onchange="getCities();">
									<option value="">Select</option>
									
								</select>
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>City *</label>
								<select name="city_id" id="city_id" class="form-control select2" data-placeholder="" style="width: 100%;" required>
									<option value="">Select</option>
									
								</select>
							</div>
						</div>
						

						
						<div class="col-md-12">
							<div class="form-group">
								<label>Tour Image *</label>
								<input type='file'  name="tour_image" onchange="readURL(this);" required>	
								<img id="blah" src="http://placehold.it/180" alt="your image" class="pre-img" />
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="form-group">
								<label>Description</label>
								<textarea id="editor1" name='description'>
								</textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Status</label>
								<select name="status" class="form-control select2" data-placeholder="" style="width: 100%;" required>
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Tour Meta Title </label>
								<input type="text" name="tour_metatitle" class="form-control" placeholder="Enter Tour Meta Title....">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>tour Meta Description </label>
								<input type="text" name="tour_metadesc" class="form-control" placeholder="Enter tour Meta Description...." >
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>tour Meta Keyword </label>
								<input type="text" name="tour_metakeyword" class="form-control" placeholder="Enter tour Meta Keywords...." >
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>tour Canonical/URL </label>
								<input type="text" name="slug" class="form-control" placeholder="Enter tour URL...." >
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>tour Schema</label>
								<input type="text" name="tour_schema" class="form-control" placeholder="Enter tour Schema...." >
							</div>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<script type="text/javascript">
	
	function getRegion() {
		tour_type = $("#tour_type").val()
		console.log(tour_type)
		if (tour_type == '2') {
			$("#country_div").css("display", "block")
		}
		else{
			
			$("#country_div").css("display", "none")
		}

		$.ajax({
			url:'<?php echo base_url().'admin/tours/getlocations' ?>',
			method: 'post',
			data: {tour_type: tour_type},
			dataType: 'json',
			success: function(response){
				console.log(response)
				$("#city_id").empty()
				$("#city_id").append('<option value="">Select</option>')

				$.each(response, function( index, value ) {
					if (tour_type == '1') {
						$("#city_id").append('<option value="'+value.region_id+'">'+value.name+'</option>')
					}
					else{
						$("#country_id").append('<option value="'+value.region_id+'">'+value.name+'</option>')
					}
				});
			}
		});

	}

	function getCities() {
		country_id = $("#country_id").val()

		$.ajax({
			url:'<?php echo base_url().'admin/tours/getlocations' ?>',
			method: 'post',
			data: {country_id: country_id},
			dataType: 'json',
			success: function(response){
				console.log(response)
				$("#city_id").empty()
				$("#city_id").append('<option value="">Select</option>')

				$.each(response, function( index, value ) {
					
					$("#city_id").append('<option value="'+value.region_id+'">'+value.name+'</option>')
					
				});
			}
		});
	}
</script>

<?php $this->load->view('layouts/footer');?>