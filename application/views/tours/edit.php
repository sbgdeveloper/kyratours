<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Tours Management
			<small>Edit Tour</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/tours/list">Tours</a></li>
			<li class="active">Edit Tour</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<div class="row">
					<form method="post" enctype="multipart/form-data">

						
						<div class="col-md-12">
							<div class="form-group">
								<label>Tour Type *</label>
								<select name="tour_type" id="tour_type" class="form-control select2" data-placeholder="" style="width: 100%;" required onchange="getRegion();">
									<option value="">Select</option>
									<?php if ($Record['tour_type'] == '1'): ?>
										<option value="1" selected>Domestic</option>
										<option value="2">International</option>
										<?php elseif ($Record['tour_type'] == '2'): ?>
											<option value="1">Domestic</option>
											<option value="2" selected>International</option>
										<?php endif ?>
									</select>
								</div>
							</div>
							<?php if ($Record['tour_type'] == '2'): ?>
							<div class="col-md-12" id="country_div">
								<div class="form-group">
									<label>Country *</label>
									<select name="country_id" id="country_id" class="form-control select2" data-placeholder="" style="width: 100%;" onchange="getCities();" required>
										<option value="">Select</option>

									</select>
								</div>
							</div>
							<?php else: ?>
							<div class="col-md-12" id="country_div" style="display: none;">
								<div class="form-group">
									<label>Country *</label>
									<select name="country_id" id="country_id" class="form-control select2" data-placeholder="" style="width: 100%;" onchange="getCities();">
										<option value="">Select</option>

									</select>
								</div>
							</div>
							<?php endif ?>

							<div class="col-md-12">
								<div class="form-group">
									<label>City *</label>
									<select name="city_id" id="city_id" class="form-control select2" data-placeholder="" style="width: 100%;" required>
										<option value="">Select</option>

									</select>
								</div>
							</div>



							<div class="col-md-12">
								<div class="form-group">
									<label>Tour Image</label>
									<input type='file'  name="tour_image" onchange="readURL(this);" value="<?=$Record['tour_image']?>" >
									<?php if (isset($Record['tour_image'])): ?>
										<img id="blah" src="<?= base_url('uploads/tours')?>/<?=$Record['tour_image']?>" alt="your image" class="pre-img" />
										<?php else: ?>
											<img id="blah" src="http://placehold.it/180" alt="your image" class="pre-img" />
										<?php endif ?>

									</div>
								</div>

								<div class="col-md-12">
									<div class="form-group">
										<label>Description</label>
										<textarea id="editor1" name='description'><?=$Record['description']?></textarea>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Status</label>
										<select name="status" class="form-control select2" data-placeholder="" style="width: 100%;" required>
											<option value="1">Active</option>
											<option value="0">Inactive</option>
										</select>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Tour Meta Title </label>
										<input type="text" name="tour_metatitle" class="form-control" placeholder="Enter Tour Meta Title...." value="<?=$Record['tour_metatitle']?>">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>tour Meta Description </label>
										<input type="text" name="tour_metadesc" class="form-control" placeholder="Enter tour Meta Description...." value="<?=$Record['tour_metadesc']?>" >
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>tour Meta Keyword </label>
										<input type="text" name="tour_metakeyword" class="form-control" placeholder="Enter tour Meta Keywords...." value="<?=$Record['tour_metakeyword']?>" >
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>tour Canonical/URL </label>
										<input type="text" name="slug" class="form-control" placeholder="Enter tour URL...." value="<?=$Record['slug']?>" >
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>tour Schema</label>
										<input type="text" name="tour_schema" class="form-control" placeholder="Enter tour Schema...." value="<?=$Record['tour_schema']?>" >
									</div>
								</div>


								<div class="col-md-12">
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</form>
						</div>
					</div>
					<!-- /.row -->
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</section>
		<!-- /.content -->
	</div>

	<script type="text/javascript">

		function getRegion() {
			tour_type = $("#tour_type").val()
			console.log(tour_type)
			if (tour_type == '2') {
				$("#country_div").css("display", "block")
			}
			else{

				$("#country_div").css("display", "none")
			}

			$.ajax({
				url:'<?php echo base_url().'admin/tours/getlocations' ?>',
				method: 'post',
				data: {tour_type: tour_type},
				dataType: 'json',
				success: function(response){
					console.log(response)
					$("#city_id").empty()
					$("#city_id").append('<option value="">Select</option>')

					$.each(response, function( index, value ) {
						if (tour_type == '1') {
							$("#city_id").append('<option value="'+value.region_id+'">'+value.name+'</option>')
						}
						else{
							$("#country_id").append('<option value="'+value.region_id+'">'+value.name+'</option>')
						}
					});
				}
			});

		}

		function getCities() {
			country_id = $("#country_id").val()

			$.ajax({
				url:'<?php echo base_url().'admin/tours/getlocations' ?>',
				method: 'post',
				data: {country_id: country_id},
				dataType: 'json',
				success: function(response){
					console.log(response)
					$("#city_id").empty()
					$("#city_id").append('<option value="">Select</option>')

					$.each(response, function( index, value ) {

						$("#city_id").append('<option value="'+value.region_id+'">'+value.name+'</option>')

					});
				}
			});
		}
	</script>

	<?php $this->load->view('layouts/footer');?>