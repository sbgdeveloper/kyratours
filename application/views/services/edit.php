<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Services Management
			<small>Edit Service</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/service/list">Services</a></li>
			<li class="active">Edit Service</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<div class="row">
					<form method="post" enctype="multipart/form-data">
						<div class="col-md-12">
							<div class="form-group">
								<label>Service Image</label>
								<input type='file'  name="service_image" onchange="readURL(this);" value="<?=$Record['service_image']?>" >
								<?php if (isset($Record['service_image'])): ?>
									<img id="blah" src="<?= base_url('uploads/services')?>/<?=$Record['service_image']?>" alt="your image" class="pre-img" />
									<?php else: ?>
										<img id="blah" src="http://placehold.it/180" alt="your image" class="pre-img" />
									<?php endif ?>

								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Service Title *</label>
									<input type="text" name="service_title" class="form-control" placeholder="Enter Service Title...." value="<?=$Record['service_title']?>">
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="form-group">
									<label>Description *</label>
									<textarea id="editor1" name='description'><?=$Record['description']?>
								</textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Status</label>
								<select name="status" class="form-control select2" data-placeholder="" style="width: 100%;" required>
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Service Meta Title *</label>
								<input type="text" name="service_metatitle" class="form-control" placeholder="Enter Service Meta Title...." value="<?=$Record['service_metatitle']?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Service Meta Description *</label>
								<input type="text" name="service_metadesc" class="form-control" placeholder="Enter Service Meta Description...." value="<?=$Record['service_metadesc']?>">
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>Service Meta Keyword </label>
								<input type="text" name="service_metakeyword" class="form-control" placeholder="Enter Service Meta Keywords...." value="<?=$Record['service_metakeyword']?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Service Canonical/URL </label>
								<input type="text" name="slug" class="form-control" placeholder="Enter Service URL...." value="<?=$Record['slug']?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Service Schema</label>
								<input type="text" name="service_schema" class="form-control" placeholder="Enter Service Schema...." value="<?=$Record['service_schema']?>">
							</div>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section>
<!-- /.content -->
</div>
<?php $this->load->view('layouts/footer');?>