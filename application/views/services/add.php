<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Services Management
			<small>Add Service</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/service/list">Services</a></li>
			<li class="active">Add Service</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<?php if($this->session->flashdata('msg')): ?>
					<div class="alert alert-info">
						<strong>Info!</strong> <?php echo $this->session->flashdata('msg') ?>
					</div>
				<?php endif ?>
				<div class="row">
					<form method="post" enctype="multipart/form-data">
						<div class="col-md-12">
							<div class="form-group">
								<label>Service Title *</label>
								<input type="text" name="service_title" class="form-control" placeholder="Enter Service Title...." required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Service Image *</label>
								<input type='file'  name="service_image" onchange="readURL(this);" required>	
								<img id="blah" src="http://placehold.it/180" alt="your image" class="pre-img" />
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="form-group">
								<label>Description *</label>
								<textarea id="editor1" name='description' required>
								</textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Status</label>
								<select name="status" class="form-control select2" data-placeholder="" style="width: 100%;" required>
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Service Meta Title </label>
								<input type="text" name="service_metatitle" class="form-control" placeholder="Enter Service Meta Title....">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Service Meta Description </label>
								<input type="text" name="service_metadesc" class="form-control" placeholder="Enter Service Meta Description...." >
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Service Meta Keyword </label>
								<input type="text" name="service_metakeyword" class="form-control" placeholder="Enter Service Meta Keywords...." >
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Service Canonical/URL </label>
								<input type="text" name="slug" class="form-control" placeholder="Enter Service URL...." >
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Service Schema</label>
								<input type="text" name="service_schema" class="form-control" placeholder="Enter Service Schema...." >
							</div>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<?php $this->load->view('layouts/footer');?>