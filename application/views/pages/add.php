<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Pages Management
			<small>Add Pages</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/pages/list">Pages</a></li>
			<li class="active">Add Page</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<?php if($this->session->flashdata('msg')): ?>
					<div class="alert alert-info">
						<strong>Info!</strong> <?php echo $this->session->flashdata('msg') ?>
					</div>
				<?php endif ?>
				<div class="row">
					<form method="post">
						<div class="col-md-12">
							<div class="form-group">
								<label>Title *</label>
								<input type="text" name="title" class="form-control" placeholder="Enter Page Title" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Description *</label>
								<textarea class="form-control" id="editor1" name='description' rows="5" placeholder="Page Content"></textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Meta Title *</label>
								<input type="text" name="seo_title" class="form-control" placeholder="Enter Meta Title" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Meta Description *</label>
								<input type="text" name="seo_description" class="form-control" placeholder="Enter Meta Description" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Meta Keyword *</label>
								<input type="text" name="seo_keyword" class="form-control" placeholder="Enter Meta Keywords" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Page Slug *</label>
								<input type="text" name="page_slug" class="form-control" placeholder="Enter Page URL" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Canonical-URL *</label>
								<input type="text" name="cannonical_link" class="form-control" placeholder="Enter Canonical URL" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Status</label>
								<select name="status" class="form-control select2" data-placeholder="" style="width: 100%;">
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<!-- <script>
	tinymce.init({
		selector:'textarea',
		inline_styles : false
	});
</script> -->
<?php $this->load->view('layouts/footer');?>