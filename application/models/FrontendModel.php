<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FrontendModel extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getAll($tbl)
	{
		$this->db->where('status', 1);
		$this->db->order_by('id','desc');
		$sql = $this->db->get($tbl);
		if ($sql->num_rows()>0)
		{
			return $sql->result_array();
		}
		else
		{
			return false;
		}
	}

	public function getTours($tbl)
	{

		$sql = $this->db->select($tbl.'.*,regions.name')
                      ->join('regions', 'regions.region_id = tours.city_id')
                      ->where($tbl.'.status', 1)
                      ->order_by('tours.id', 'DESC')
                      ->get('tours');

		// $this->db->where('status', 1);
		// $this->db->order_by('id','desc');
		// $sql = $this->db->get($tbl);
		if ($sql->num_rows()>0)
		{
			return $sql->result_array();
		}
		else
		{
			return false;
		}
	}

	public function getById($tbl, $type)
	{
		if ($type == 'domestic') {
			$tour_type = 1;
		}
		else{
			$tour_type = 2;
		}
		
		$sql = $this->db->select($tbl.'.*,regions.name')
                      ->join('regions', 'regions.region_id = tours.city_id')
                      ->where($tbl.'.status', 1)
                      ->where($tbl.'.tour_type', $tour_type)
                      ->order_by('tours.id', 'DESC')
                      ->get('tours');
		
		// $sql= $this->db->get($table);
		if ($sql->num_rows()>0)
		{
			return $sql->result_array();
		}
		else
		{
			return false;
		}
	}

	public function add($Array, $table)
	{
		if ($this->db->insert($table, $Array))
		{
			return $this->db->insert_id();
		}
		else
		{
			return false;
		}
	}

	public function edit($Array, $table, $id)
	{
		$this->db->where('id', $id);
		if ($this->db->update($table, $Array))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function delete($table, $id)
	{
		if ($this->db->delete($table, array('id' => $id)))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function enable($table, $id)
	{
		$this->db->where('id', $id);
		if ($this->db->update($table, array('status' => 1)))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function disable($table, $id)
	{
		$this->db->where('id', $id);
		if ($this->db->update($table, array('status' => 0)))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function record_count($tbl) {
		return $this->db->count_all($tbl);
	}

	public function get_pagi($limit,$start, $tbl) {
		$this->db->limit($limit, $start);
		$this->db->order_by('id', 'DESC');
		$sql = $this->db->get($tbl);
		if ($sql->num_rows() > 0) {
			return $sql->result_array();
		} else {
			return false;
		}
	}

}