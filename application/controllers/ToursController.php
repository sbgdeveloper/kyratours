<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ToursController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('TourModel');
		$this->load->model('RegionModel');
		
		$this->load->library('upload');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			$data['Records'] = $this->TourModel->getAll('tours'); 
			
			$data['pagetitle'] = 'Tours List';
			$this->load->view('tours/list', $data);
		}else{
			redirect(base_url().'login');
		}
	}
	
	public function add()
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/tours/';
				$config['allowed_types'] = 'jpeg|jpg|png|gif';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('tour_image'))
				{
					$picture = $this->upload->data()['file_name'];
				}
				else
				{
					$picture = NULL;
				}

				$data = array(
					'tour_type' =>$this->input->post('tour_type') , 
					'country_id' => empty($this->input->post('country_id')) ? 107 : $this->input->post('country_id') , 
					'city_id' =>$this->input->post('city_id') , 
					'description' =>$this->input->post('description') , 
					'status' =>$this->input->post('status') , 
					'tour_image' =>$picture , 
					'tour_metatitle' =>$this->input->post('tour_metatitle') , 
					'tour_metadesc' =>$this->input->post('tour_metadesc') , 
					'tour_metakeyword' =>$this->input->post('tour_metakeyword') , 
					'slug' =>$this->input->post('slug') , 
					'tour_schema' =>$this->input->post('tour_schema') , 
					'created_at'=>date("Y-m-d H:i:s"),
					'created_by' => $this->session->userdata('username'), 

				);
				if ($this->TourModel->add($data, 'tours'))
				{
					$this->session->set_flashdata('msg', 'Record Added Successfully');
					redirect(base_url().'admin/tours/list');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Adding Record');
					redirect(base_url().'admin/tours/add');
				}


			}else{
				$data['pagetitle'] = 'Add Tour';
				$data['Records'] = $this->TourModel->getAll('tours');
				$this->load->view('tours/add', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}


	public function edit($id)
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/tours/';
				$config['allowed_types'] = 'jpeg|jpg|png|gif';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('tour_image'))
				{
					$picture = $this->upload->data()['file_name'];
					$data = array(
						'tour_type' =>$this->input->post('tour_type') , 
						'country_id' => empty($this->input->post('country_id')) ? 107 : $this->input->post('country_id') , 
						'city_id' =>$this->input->post('city_id') , 
						'description' =>$this->input->post('description') , 
						'status' =>$this->input->post('status') , 
						'tour_image' =>$picture , 
						'tour_metatitle' =>$this->input->post('tour_metatitle') , 
						'tour_metadesc' =>$this->input->post('tour_metadesc') , 
						'tour_metakeyword' =>$this->input->post('tour_metakeyword') , 
						'slug' =>$this->input->post('slug') , 
						'tour_schema' =>$this->input->post('tour_schema') ,
						'updated_at'=>date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('username'), 
					);
				}
				else
				{
					$data = array(
						'tour_type' =>$this->input->post('tour_type') , 
						'country_id' => empty($this->input->post('country_id')) ? 107 : $this->input->post('country_id') , 
						'city_id' =>$this->input->post('city_id') , 
						'description' =>$this->input->post('description') , 
						'status' =>$this->input->post('status') , 

						'tour_metatitle' =>$this->input->post('tour_metatitle') , 
						'tour_metadesc' =>$this->input->post('tour_metadesc') , 
						'tour_metakeyword' =>$this->input->post('tour_metakeyword') , 
						'slug' =>$this->input->post('slug') , 
						'tour_schema' =>$this->input->post('tour_schema') ,
						'updated_at'=>date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('username'), 

					);
				}
				if ($this->TourModel->edit($data, 'tours', $id))
				{
					$this->session->set_flashdata('msg', 'Record Edited Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Editing Record');
				}
				redirect(base_url().'admin/tours/list');

			}else{
				$data['pagetitle'] = 'Edit Tour';
				$data['Record'] = $this->TourModel->getById('tours', $id);
				// $data['services'] = $this->ServiceModel->getAll('services');
				$this->load->view('tours/edit', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function delete($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->ServiceComponentModel->delete('servicecomponents', $id))
			{
				$this->session->set_flashdata('msg', 'Record Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Deleting Record');
			}
			redirect(base_url().'admin/service-component/list');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function enable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->ServiceComponentModel->enable('servicecomponents', $id))
			{
				$this->session->set_flashdata('msg', 'Record Enabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Enabling Record');
			}
			redirect(base_url().'admin/service-component/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function disable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->ServiceComponentModel->disable('servicecomponents', $id))
			{
				$this->session->set_flashdata('msg', 'Record Disabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Disabling Record');
			}
			redirect(base_url().'admin/service-component/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function getlocations()
	{

		if (!empty($this->input->post('country_id'))) {
			$country_id = $this->input->post('country_id');
			$data = $this->RegionModel->getByRegionId('regions', $country_id);
			echo json_encode($data);
		}
		else{
			$tour_type = $this->input->post('tour_type');

			if ($tour_type == '1') {
				$data = $this->RegionModel->getByRegionId('regions', 107);
				echo json_encode($data);
			}
			else{
				$data = $this->RegionModel->getInternationalCities('regions');
				echo json_encode($data);
			}
		}

		// $data['Record'] = $this->ServiceComponentModel->getById('servicecomponents', $id);
	}

}
