<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('FrontendModel');
	}
	
	public function index()	{
		$data['seo_title'] = "Home";
		$data['seo_description'] = "Home";
		$data['seo_keywords'] = "Home";
		$data['cannonical_link'] = base_url();
		$data['tours'] = $this->FrontendModel->getTours('tours');
		$this->load->view('frontend/home', $data);
	}

	public function tour($type)	{
		$data['seo_title'] = $type;
		$data['seo_description'] = $type;
		$data['seo_keywords'] = $type;
		$data['cannonical_link'] = base_url('tours/'.$type);
		$data['tours'] = $this->FrontendModel->getById('tours', $type);
		$data['type'] = ucfirst($type);
		$this->load->view('frontend/locations', $data);
	}

	public function aboutUs()	{
		$data['seo_title'] = "About us";
		$data['seo_description'] = "About us";
		$data['seo_keywords'] = "About us";
		$data['cannonical_link'] = base_url('about-us');
		$data['testimonials'] = $this->FrontendModel->getAll('testimonials'); 
		$this->load->view('frontend/aboutus', $data);
	}

	public function flight()
	{
		$data['seo_title'] = "Flight";
		$data['seo_description'] = "Flight";
		$data['seo_keywords'] = "Flight";
		$data['cannonical_link'] = base_url('flight');
		$data['tours'] = $this->FrontendModel->getTours('tours');
		$this->load->view('frontend/flight', $data);
	}

	public function carrental()
	{
		$data['seo_title'] = "Car rental";
		$data['seo_description'] = "Car rental";
		$data['seo_keywords'] = "Car rental";
		$data['cannonical_link'] = base_url('car-rental');
		// $data['tours'] = $this->FrontendModel->getTours('tours');
		$this->load->view('frontend/carrental', $data);
	}

	public function visa()
	{
		$data['seo_title'] = "Visa";
		$data['seo_description'] = "Visa";
		$data['seo_keywords'] = "Visa";
		$data['cannonical_link'] = base_url('visa');
		// $data['tours'] = $this->FrontendModel->getTours('tours');
		$this->load->view('frontend/visa', $data);
	}

	public function interpriseTickets()
	{
		$data['seo_title'] = "interprise Tickets";
		$data['seo_description'] = "interpriseTickets";
		$data['seo_keywords'] = "interpriseTickets";
		$data['cannonical_link'] = base_url('interprise-tickets');
		// $data['tours'] = $this->FrontendModel->getTours('tours');
		$this->load->view('frontend/interprise-tickets', $data);
	}

	public function interpriseVisa()
	{
		$data['seo_title'] = "interprise Visa";
		$data['seo_description'] = "interprise Visa";
		$data['seo_keywords'] = "interprise Visa";
		$data['cannonical_link'] = base_url('interprise-visa');
		// $data['tours'] = $this->FrontendModel->getTours('tours');
		$this->load->view('frontend/interprise-visa', $data);
	}

	public function virtualDesk()
	{
		$data['seo_title'] = "virtual Desk";
		$data['seo_description'] = "virtual Desk";
		$data['seo_keywords'] = "virtual Desk";
		$data['cannonical_link'] = base_url('virtual-desk');
		// $data['tours'] = $this->FrontendModel->getTours('tours');
		$this->load->view('frontend/virtual-desk', $data);
	}

	public function bookNow()
	{
		$data['seo_title'] = "book Now";
		$data['seo_description'] = "book Now";
		$data['seo_keywords'] = "book Now";
		$data['cannonical_link'] = base_url('book-now');
		// $data['tours'] = $this->FrontendModel->getTours('tours');
		$this->load->view('frontend/book-now', $data);
	}

	

	


	public function enquiry()	{
		
			$this->load->config('email');
			$this->load->library('email');

			$to = $this->config->item('smtp_user');
			// $to = 'jayash.n@straitsresearch.net';
			$from = $this->input->post('email');
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject("Contact Us Form: Enquiry");

			$maildata = "
			Hello There E- Mail For You,<br>
			<table>
			<tr>
			<td><b>Name</b> : </td>
			<td>".$this->input->post('name')."</td>
			</tr>
			<tr>
			<td><b>Email</b> : </td>
			<td>".$this->input->post('email')."</td>
			</tr>
			<tr>
			<td><b>Phone</b> : </td>
			<td>".$this->input->post('phone')."</td>
			</tr>
			<tr>
			<td><b>City</b> : </td>
			<td>".$this->input->post('city')."</td>
			</tr>
			<tr>
			<td><b>Destination</b> : </td>
			<td>".$this->input->post('destination')."</td>
			</tr>
			<tr>
			<td><b>Message</b> : </td>
			<td>".$this->input->post('msg')."</td>
			</tr>
			</table>";


			$this->email->set_newline("\r\n");
			$this->email->message($maildata);
			
			if($this->email->send()) 
				$this->session->set_flashdata("email_sent","We have received your enquiry. Will get back to you soon."); 
			else 
				$this->session->set_flashdata("email_sent_err","Error in sending Email."); 
			
			redirect($_SERVER['HTTP_REFERER']);
		
	}

	public function subscribe()	{
		
			$this->load->config('email');
			$this->load->library('email');

			$to = $this->config->item('smtp_user');
			// $to = 'jayash.n@straitsresearch.net';
			$from = $this->input->post('email');
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject("Subscribe To Newsletter");

			$maildata = "
			Hello There E- Mail For You,<br>
			<table>
			
			<tr>
			<td><b>Email</b> : </td>
			<td>".$this->input->post('email')."</td>
			</tr>
			
			</table>";


			$this->email->set_newline("\r\n");
			$this->email->message($maildata);
			
			if($this->email->send()) 
				$this->session->set_flashdata("email_subscribe","Thank you for your interest..!"); 
			else 
				$this->session->set_flashdata("email_subscribe_err","Error in sending Email."); 
			
			redirect($_SERVER['HTTP_REFERER']);
		
	}

	public function corporate()
	{
		$data['seo_title'] = "corporate travel";
		$data['seo_description'] = "corporate travel";
		$data['seo_keywords'] = "corporate travel";
		$data['cannonical_link'] = base_url('corporate-travel');		
		$this->load->view('frontend/corporate', $data);
	}

	public function insurance()
	{
		$data['seo_title'] = "travel insurance";
		$data['seo_description'] = "travel insurance";
		$data['seo_keywords'] = "travel insurance";
		$data['cannonical_link'] = base_url('travel-insurance');		
		$this->load->view('frontend/insurance', $data);
	}

	public function contactUs()	{
		if($this->input->post()){
			$this->load->config('email');
			$this->load->library('email');

			// $to = $this->config->item('smtp_user');
			$to = 'jayash.n@straitsresearch.net';
			$from = $this->input->post('email');
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject("Contact Us Form: Enquiry");

			$maildata = "
			Hello There E- Mail For You,<br>
			<table>
			<tr>
			<td><b>Name</b> : </td>
			<td>".$this->input->post('name')."</td>
			</tr>
			<tr>
			<td><b>Email</b> : </td>
			<td>".$this->input->post('email')."</td>
			</tr>
			<tr>
			<td><b>Phone</b> : </td>
			<td>".$this->input->post('phone')."</td>
			</tr>
			<tr>
			<td><b>City</b> : </td>
			<td>".$this->input->post('city')."</td>
			</tr>
			<tr>
			<td><b>Destination</b> : </td>
			<td>".$this->input->post('destination')."</td>
			</tr>
			<tr>
			<td><b>Message</b> : </td>
			<td>".$this->input->post('msg')."</td>
			</tr>
			</table>";


			$this->email->set_newline("\r\n");
			$this->email->message($maildata);
			
			if($this->email->send()) 
				$this->session->set_flashdata("email_sent","Thank you for contacting us..!"); 
			else 
				$this->session->set_flashdata("email_sent_err","Error in sending Email."); 
			
			redirect($_SERVER['HTTP_REFERER']);
		}
		else{
			
			$data['seo_title'] = "Contact Us";
			$data['seo_description'] = "Contact Us";
			$data['seo_keywords'] = "Contact Us";
			$data['cannonical_link'] = base_url('contact-us');
			$this->load->view('frontend/contactus', $data);
		}
	}

	

	public function downloadFiles(){
		$data['pagetitle'] = "Download Files";
		$data['downloads'] = $this->FrontendModel->getAll('download','asc'); 
		$this->load->view('frontend/download', $data);
	}


	public function EnqForm(){
		if($this->input->post()){
			$this->load->config('email');
			$this->load->library('email');

			$to = $this->config->item('smtp_user');
			$from = $this->input->post('email');
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject("Enquiry From: Enquiry");

			$maildata = "
			Hello There E- Mail For You,<br>
			<table>
			<tr>
			<td><b>Name</b> : </td>
			<td>".$this->input->post('name')."</td>
			</tr>
			<tr>
			<td><b>Email</b> : </td>
			<td>".$this->input->post('email')."</td>
			</tr>
			<tr>
			<td><b>Phone</b> : </td>
			<td>".$this->input->post('phone')."</td>
			</tr>
			<tr>
			<td><b>Message</b> : </td>
			<td>".$this->input->post('msg')."</td>
			</tr>
			</table>";


			$this->email->set_newline("\r\n");
			$this->email->message($maildata);

			if($this->email->send()) 
				$this->session->set_flashdata("email_sent","Email sent successfully."); 
			else 
				$this->session->set_flashdata("email_sent_err","Error in sending Email."); 

			redirect($_SERVER['HTTP_REFERER']);

		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	
}