<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TestimonialsController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('TestimonialsModel');
		$this->load->library('upload');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			$data['Records'] = $this->TestimonialsModel->getAll('testimonials'); 
			$data['pagetitle'] = 'Testimonial List';
			$this->load->view('testimonials/list', $data);
		}else{
			redirect(base_url().'login');
		}
	}
	
	public function add()
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/testimonials/';
				$config['allowed_types'] = 'jpeg|jpg|png|gif';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('image'))
				{
					$picture = $this->upload->data()['file_name'];
				}
				else
				{
					$picture = NULL;
				}

				$data = array(
					'name' =>$this->input->post('name') , 
					'image' =>$picture , 
					'title' =>$this->input->post('title') , 
					'company' =>$this->input->post('company') , 
					'description' =>$this->input->post('description') , 
					'status' =>$this->input->post('status') , 
					'created_at'=>date("Y-m-d H:i:s"),
					'created_by' => $this->session->userdata('username'), 

				);
				if ($this->TestimonialsModel->add($data, 'testimonials'))
				{
					$this->session->set_flashdata('msg', 'Testimonial Added Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Adding Testimonial');
				}

				redirect(base_url().'admin/testimonials/add');

			}else{
				$data['pagetitle'] = 'Add Testimonials';
				$this->load->view('testimonials/add', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}


	public function edit($id)
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/testimonials/';
				$config['allowed_types'] = 'jpeg|jpg|png|gif';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('image'))
				{
					$picture = $this->upload->data()['file_name'];
					$data = array(
						'name' =>$this->input->post('name') , 
						'image' =>$picture , 
						'title' =>$this->input->post('title') , 
						'company' =>$this->input->post('company') , 
						'description' =>$this->input->post('description') , 
						'updated_at'=>date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('username'), 

					);
				}
				else
				{
					$data = array(
						'name' =>$this->input->post('name') , 
						'title' =>$this->input->post('title') , 
						'company' =>$this->input->post('company') , 
						'description' =>$this->input->post('description') , 
						'updated_at'=>date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('username'), 

					);
				}


				
				if ($this->TestimonialsModel->edit($data, 'testimonials', $id))
				{
					$this->session->set_flashdata('msg', 'Testimonial Edited Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Editing Testimonial');
				}
				redirect(base_url().'admin/testimonials/list');

			}else{
				$data['pagetitle'] = 'Edit Testimonial';
				$data['Record'] = $this->TestimonialsModel->getById('testimonials', $id);
				$this->load->view('testimonials/edit', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function delete($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->TestimonialsModel->delete('testimonials', $id))
			{
				$this->session->set_flashdata('msg', 'Testimonial Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Deleting Testimonial');
			}
			redirect(base_url().'admin/testimonials/list');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function enable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->TestimonialsModel->enable('testimonials', $id))
			{
				$this->session->set_flashdata('msg', 'Testimonial Enabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Enabling Testimonial');
			}
			redirect(base_url().'admin/testimonials/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function disable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->TestimonialsModel->disable('testimonials', $id))
			{
				$this->session->set_flashdata('msg', 'Testimonial Disabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Disabling Testimonial');
			}
			redirect(base_url().'admin/testimonials/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

}
