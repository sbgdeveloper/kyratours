-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: kyratours
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_title` text NOT NULL,
  `category` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `blog_image` varchar(200) DEFAULT NULL,
  `blog_metatitle` text NOT NULL,
  `blog_metadesc` text NOT NULL,
  `blog_metakeyword` text NOT NULL,
  `slug` varchar(100) NOT NULL,
  `blog_schema` text NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogs`
--

LOCK TABLES `blogs` WRITE;
/*!40000 ALTER TABLE `blogs` DISABLE KEYS */;
/*!40000 ALTER TABLE `blogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `seo_keywords` text COLLATE utf8mb4_unicode_ci,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_by` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (2,'technology','test.png','<p>technology</p>\r\n','technology','technology','technology','technology','technology',1,'admin','admin','2020-04-10 18:02:20','2020-04-10 18:08:04');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(2) DEFAULT '1',
  `created_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (2,'Download Management','test.png','pune',1,'admin',NULL,'2020-04-10 20:25:09',NULL);
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `gallery_image` varchar(200) DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1=active, 0=inactive',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(45) NOT NULL,
  `customer_phone` varchar(45) NOT NULL,
  `building_width` float NOT NULL,
  `building_length` float NOT NULL,
  `building_height` float NOT NULL,
  `location` text,
  `supply_price` varchar(45) NOT NULL,
  `erection_price` varchar(45) NOT NULL,
  `total_price` varchar(45) NOT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_page_slug_unique` (`page_slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'About Us','Lorem-Ipsum','<p>What is Lorem Ipsum? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n','Lorem-Ipsum','desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum','desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum','desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',1,'0','test','2020-04-09 00:00:00','2020-04-09 00:00:00');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `seo_keywords` text COLLATE utf8mb4_unicode_ci,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_by` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'test','test.png','<p>test123</p>\r\n','test','test','test','test','test',1,'admin','admin','2020-04-10 19:27:58','2020-04-10 19:28:32');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regions`
--

DROP TABLE IF EXISTS `regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regions` (
  `region_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` int(11) DEFAULT '2' COMMENT '1=country, 2=continent, 3=global',
  `parent` int(11) DEFAULT '0',
  `super_parent` int(11) DEFAULT '1',
  `status` enum('Active','InActive') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status1` enum('Active','InActive') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`region_id`)
) ENGINE=InnoDB AUTO_INCREMENT=270 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regions`
--

LOCK TABLES `regions` WRITE;
/*!40000 ALTER TABLE `regions` DISABLE KEYS */;
INSERT INTO `regions` VALUES (1,'Global','2019-03-22 01:13:25','2019-03-22 01:13:25',1,0,0,NULL,NULL,NULL,NULL),(2,'North America','2019-03-22 01:13:25','2019-03-22 01:13:25',1,1,0,NULL,NULL,NULL,NULL),(3,'Europe','2019-03-22 01:13:25','2019-03-22 01:13:25',1,1,0,NULL,NULL,NULL,NULL),(4,'Asia Pacific','2019-03-22 01:13:25','2019-03-22 01:13:25',1,1,0,NULL,NULL,NULL,NULL),(5,'South America','2019-03-22 01:13:25','2019-03-22 01:13:25',1,1,0,NULL,NULL,NULL,NULL),(6,'Middle East And Africa','2019-03-22 01:13:25','2019-03-22 01:13:25',1,1,0,NULL,NULL,NULL,NULL),(7,'Afghanistan','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(8,'Aland Islands','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(9,'Albania','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(10,'Algeria','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(11,'American Samoa','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(12,'Andorra','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(13,'Angola','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(14,'Anguilla','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(15,'Antarctica','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(16,'Antigua and Barbuda','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(17,'Argentina','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(18,'Armenia','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(19,'Aruba','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(20,'Australia','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(21,'Austria','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(22,'Azerbaijan','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(23,'Bahamas','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(24,'Bahrain','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(25,'Bangladesh','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(26,'Barbados','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(27,'Belarus','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(28,'Belgium','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(29,'Belize','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(30,'Benin','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(31,'Bermuda','2019-10-21 01:13:25',NULL,2,2,1,NULL,NULL,NULL,NULL),(32,'Bhutan','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(33,'Bolivia','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(34,'Bosnia and Herzegovina','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(35,'Botswana','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(36,'Bouvet Island','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(37,'Brazil','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(38,'British Indian Ocean Territory','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(39,'Brunei Darussalam','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(40,'Bulgaria','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(41,'Burkina Faso','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(42,'Burundi','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(43,'Cambodia','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(44,'Cameroon','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(45,'Canada','2019-10-21 01:13:25',NULL,2,2,1,NULL,NULL,NULL,NULL),(46,'Cape Verde','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(47,'Cayman Islands','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(48,'Central african Republic','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(49,'Chad','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(50,'Chile','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(51,'China','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(52,'Christmas Island','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(53,'Cocos (Keeling), Islands','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(54,'Colombia','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(55,'Comoros','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(56,'Congo','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(57,'Congo, The Democratic Republic of the','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(58,'Cook Islands','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(59,'Costa Rica','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(60,'Côte D\'Ivoire','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(61,'Croatia','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(62,'Cuba','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(63,'Cyprus','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(64,'Czech Republic','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(65,'Denmark','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(66,'Djibouti','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(67,'Dominica','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(68,'Dominican Republic','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(69,'Ecuador','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(70,'Egypt','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(71,'El Salvador','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(72,'Equatorial Guinea','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(73,'Eritrea','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(74,'Estonia','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(75,'Ethiopia','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(76,'Falkland Islands (Malvinas)','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(77,'Faroe Islands','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(78,'Fiji','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(79,'Finland','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(80,'France','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(81,'French Guiana','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(82,'French Polynesia','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(83,'French Southern Territories','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(84,'Gabon','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(85,'Gambia','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(86,'Georgia','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(87,'Germany','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(88,'Ghana','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(89,'Gibraltar','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(90,'Greece','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(91,'Greenland','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(92,'Grenada','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(93,'Guadeloupe','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(94,'Guam','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(95,'Guatemala','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(96,'Guernsey','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(97,'Guinea','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(98,'Guinea-Bissau','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(99,'Guyana','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(100,'Haiti','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(101,'Heard Island and McDonald Islands','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(102,'Holy See (Vatican City State)','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(103,'Honduras','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(104,'Hong Kong','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(105,'Hungary','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(106,'Iceland','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(107,'India','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(108,'Indonesia','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(109,'Iran, Islamic Republic of','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(110,'Iraq','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(111,'Ireland','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(112,'Isle of Man','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(113,'Israel','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(114,'Italy','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(115,'Jamaica','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(116,'Japan','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(117,'Jersey','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(118,'Jordan','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(119,'Kazakhstan','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(120,'Kenya','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(121,'Kiribati','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(122,'North Korea','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(123,'South Korea','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(124,'Kuwait','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(125,'Kyrgyzstan','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(126,'Lao People\'s Democratic Republic','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(127,'Latvia','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(128,'Lebanon','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(129,'Lesotho','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(130,'Liberia','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(131,'Libya','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(132,'Liechtenstein','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(133,'Lithuania','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(134,'Luxembourg','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(135,'Macau','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(136,'Macedonia','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(137,'Madagascar','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(138,'Malawi','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(139,'Malaysia','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(140,'Maldives','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(141,'Mali','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(142,'Malta','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(143,'Marshall Islands','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(144,'Martinique','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(145,'Mauritania','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(146,'Mauritius','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(147,'Mayotte','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(148,'Mexico','2019-10-21 01:13:25',NULL,2,2,1,NULL,NULL,NULL,NULL),(149,'Micronesia, Federated States of','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(150,'Moldova, Republic of','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(151,'Monaco','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(152,'Mongolia','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(153,'Montenegro','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(154,'Montserrat','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(155,'Morocco','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(156,'Mozambique','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(157,'Myanmar','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(158,'Namibia','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(159,'Nauru','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(160,'Nepal','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(161,'Netherlands','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(162,'Netherlands Antilles','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(163,'New Caledonia','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(164,'New Zealand','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(165,'Nicaragua','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(166,'Niger','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(167,'Nigeria','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(168,'Niue','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(169,'Norfolk Island','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(170,'Northern Mariana Islands','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(171,'Norway','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(172,'Oman','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(173,'Pakistan','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(174,'Palau','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(175,'Palestinian Territory','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(176,'Panama','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(177,'Papua New Guinea','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(178,'Paraguay','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(179,'Peru','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(180,'Philippines','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(181,'Pitcairn Islands','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(182,'Poland','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(183,'Portugal','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(184,'Puerto Rico','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(185,'Qatar','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(186,'Reunion','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(187,'Romania','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(188,'Russian Federation','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(189,'Rwanda','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(190,'Saint Helena','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(191,'Saint Kitts and Nevis','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(192,'Saint Lucia','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(193,'Saint Pierre and Miquelon','2019-10-21 01:13:25',NULL,2,2,1,NULL,NULL,NULL,NULL),(194,'Saint Vincent and the Grenadines','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(195,'Samoa','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(196,'San Marino','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(197,'Sao Tome and Principe','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(198,'Saudi Arabia','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(199,'Senegal','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(200,'Serbia','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(201,'Seychelles','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(202,'Sierra Leone','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(203,'Singapore','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(204,'Slovakia','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(205,'Slovenia','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(206,'Solomon Islands','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(207,'Somalia','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(208,'South Africa','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(209,'South Georgia and the South Sandwich Islands','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(210,'Spain','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(211,'Sri Lanka','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(212,'Sudan','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(213,'Suriname','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(214,'Svalbard and Jan Mayen','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(215,'Swaziland','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(216,'Sweden','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(217,'Switzerland','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(218,'Syrian Arab Republic','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(219,'Taiwan','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(220,'Tajikistan','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(221,'Tanzania, United Republic of','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(222,'Thailand','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(223,'Timor-Leste','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(224,'Togo','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(225,'Tokelau','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(226,'Tonga','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(227,'Trinidad and Tobago','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(228,'Tunisia','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(229,'Turkey','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(230,'Turkmenistan','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(231,'Turks and Caicos Islands','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(232,'Tuvalu','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(233,'Uganda','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(234,'Ukraine','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(235,'United Arab Emirates','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(236,'United Kingdom','2019-10-21 01:13:25',NULL,2,3,1,NULL,NULL,NULL,NULL),(237,'United States','2019-10-21 01:13:25',NULL,2,2,1,NULL,NULL,NULL,NULL),(238,'United States Minor Outlying Islands','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(239,'Uruguay','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(240,'Uzbekistan','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(241,'Vanuatu','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(242,'Venezuela','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(243,'Vietnam','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(244,'Virgin Islands, British','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(245,'Virgin Islands, U.S.','2019-10-21 01:13:25',NULL,2,5,1,NULL,NULL,NULL,NULL),(246,'Wallis and Futuna','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(247,'Western Sahara','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(248,'Yemen','2019-10-21 01:13:25',NULL,2,4,1,NULL,NULL,NULL,NULL),(249,'Zambia','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(250,'Zimbabwe','2019-10-21 01:13:25',NULL,2,6,1,NULL,NULL,NULL,NULL),(251,'Delhi','2019-10-21 01:13:25',NULL,3,107,4,NULL,NULL,NULL,NULL),(252,'Mumbai','2019-10-21 01:13:25',NULL,3,107,4,NULL,NULL,NULL,NULL),(253,'Beijing','2019-10-21 01:13:25',NULL,3,51,4,NULL,NULL,NULL,NULL),(254,'Shanghai','2019-10-21 01:13:25',NULL,3,51,4,NULL,NULL,NULL,NULL),(255,'Toronto','2019-10-21 01:13:25',NULL,3,45,2,NULL,NULL,NULL,NULL),(256,'Madrid','2019-10-21 01:13:25',NULL,3,237,2,NULL,NULL,NULL,NULL),(257,'Barcelona','2019-10-21 02:46:53',NULL,3,237,2,NULL,NULL,1,NULL),(258,'Paris','2019-10-21 02:46:53',NULL,3,237,2,NULL,NULL,1,NULL),(259,'Lille','2019-10-21 02:46:53',NULL,3,237,2,NULL,NULL,1,NULL),(260,'Boston','2019-10-21 02:46:53',NULL,3,237,2,NULL,NULL,1,NULL),(261,'Washington DC','2019-10-21 02:46:53',NULL,3,237,2,NULL,NULL,1,NULL),(262,'Chicago','2019-12-09 01:57:49','2019-12-09 01:57:49',3,237,2,NULL,NULL,1,NULL),(263,'Dallas','2019-12-09 02:41:18','2019-12-09 02:41:18',3,237,2,NULL,NULL,1,NULL),(264,'Rest Of Europe','2019-03-22 01:13:25','2019-03-22 01:13:25',2,3,1,NULL,NULL,NULL,NULL),(265,'Rest Of Asia Pacific','2019-03-22 01:13:25','2019-03-22 01:13:25',2,4,1,NULL,NULL,NULL,NULL),(266,'Rest Of South America','2019-03-22 01:13:25','2019-03-22 01:13:25',2,5,1,NULL,NULL,NULL,NULL),(267,'Rest Of Middle East And Africa','2019-03-22 01:13:25','2019-03-22 01:13:25',2,6,1,NULL,NULL,NULL,NULL),(268,'New York','2020-02-11 01:07:52','2020-02-11 01:07:52',3,237,2,NULL,NULL,1,NULL),(269,'Pune','2020-02-12 01:42:35','2020-02-12 01:42:35',3,107,4,NULL,NULL,1,NULL);
/*!40000 ALTER TABLE `regions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_title` text NOT NULL,
  `description` longtext NOT NULL,
  `service_image` varchar(200) DEFAULT NULL,
  `service_metatitle` text,
  `service_metadesc` text,
  `service_metakeyword` text,
  `slug` varchar(100) DEFAULT NULL,
  `service_schema` text,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1=active, 0=inactive',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` text,
  `email` varchar(50) DEFAULT NULL,
  `phone` text,
  `address` text,
  `url` text,
  `header_files` longtext,
  `facebook` text,
  `linkdin` text,
  `twitter` text,
  `google_plus` text,
  `instagram` text,
  `status` tinyint(2) DEFAULT NULL COMMENT '0=inactive,1=active',
  `created_at` datetime DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'test','test','35456','test','test','test','test','test','test','test','test',1,'2020-04-09 00:00:00',NULL,'admin',NULL);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcategory`
--

DROP TABLE IF EXISTS `subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcategory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `seo_keywords` text COLLATE utf8mb4_unicode_ci,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_by` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcategory`
--

LOCK TABLES `subcategory` WRITE;
/*!40000 ALTER TABLE `subcategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tours`
--

DROP TABLE IF EXISTS `tours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_type` int(11) NOT NULL COMMENT '1=domestic, 2=international',
  `country_id` int(11) NOT NULL DEFAULT '107',
  `city_id` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1=active, 0=inactive',
  `slug` varchar(100) DEFAULT NULL,
  `tour_image` varchar(200) DEFAULT NULL,
  `tour_metatitle` text,
  `tour_metadesc` text,
  `tour_metakeyword` text,
  `tour_schema` text,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tours`
--

LOCK TABLES `tours` WRITE;
/*!40000 ALTER TABLE `tours` DISABLE KEYS */;
INSERT INTO `tours` VALUES (1,1,107,269,'<p>sdfsdf testtt</p>\r\n',1,'','1550650360China.jpg','','','','','2020-04-30','2020-05-01','admin','admin');
/*!40000 ALTER TABLE `tours` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `last_login` datetime NOT NULL,
  `status` tinyint(2) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','admin@common.com','d404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db','2020-05-01 10:30:05',1,'2019-10-08'),(3,'test','test@test.com','d404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db','2020-04-10 10:06:37',1,'2020-04-09');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-01 12:37:13
